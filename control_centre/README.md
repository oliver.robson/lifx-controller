# LifX Controller Control Centre

Main controller that actually talks to the LEDs to configure them.

To run:

- `poetry install`
- `poetry run uvicorn main:app --host 0.0.0.0 --port 80`
