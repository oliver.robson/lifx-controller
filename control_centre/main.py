from typing import List
from fastapi import FastAPI
from lifxlan import device, LifxLAN

app = FastAPI()

NUM_LIGHTS = 2
DURATION = 1


@app.get("/test")
def test():
    return {"success": True}


@app.get("/leds/toggle")
def toggle_leds():
    lan = LifxLAN(NUM_LIGHTS)
    lights: List[device.Device] = lan.get_lights()
    lights_to_set = len(lights)
    if lights_to_set < 1:
        print("No lights found, bailing")
        return

    power = lights[0].get_power()
    enable = not bool(power)
    lan.set_power_all_lights(enable, duration=DURATION)
    print(f"Set {lights_to_set} lights to {'on' if enable else 'off'}")
