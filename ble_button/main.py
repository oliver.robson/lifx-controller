import bluetooth
import esp32
from machine import deepsleep, DEEPSLEEP, Pin, reset_cause
from micropython import const
import time

import m5stack

ADVERTISE_TIME_MS = const(2 * 1000)
SLEEP_DELAY_MS = const(3 * 1000)

FINGERPRINT = b"\x49\x62\x84\xef"

advertising = False

class Advertiser(object):
    def __init__(self):
        self.advertising = False
        self.ble = bluetooth.BLE()
        self.ble.active(True)

    def send_advertisement(self):
        print(f"Advertising for {ADVERTISE_TIME_MS} ms!")
        self.ble.gap_advertise(624, adv_data=FINGERPRINT, connectable=False)  # 624 = max?
        started = time.ticks_ms()
        while time.ticks_diff(time.ticks_ms(), started) < ADVERTISE_TIME_MS:
            pass
        self.ble.gap_advertise(None)

    def teardown(self):
        self.ble.active(False)

if reset_cause() == DEEPSLEEP:
    # Awoke from deepsleep, button must have been pressed
    adv = Advertiser()
    adv.send_advertisement()
    adv.teardown()

button = Pin(m5stack.BUTTON_A_PIN, Pin.IN, Pin.PULL_UP)
esp32.wake_on_ext0(pin=button, level=esp32.WAKEUP_ALL_LOW)

print(f"Going to deep sleep after {SLEEP_DELAY_MS} ms")
time.sleep_ms(SLEEP_DELAY_MS)
print("Going to deep sleep")
deepsleep()
