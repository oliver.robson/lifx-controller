# BLE Listener

You'll also need a `config.py` file on the board, with the following variables set:

```python
NETWORK_SSID = "Your WiFi SSID"
NETWORK_PASS = "Your WiFi password"
SERVER_ADDR = "http://yourlocalserverip/"
FINGERPRINT = b"someuniquefingerprint"
```
