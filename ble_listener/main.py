import bluetooth
from micropython import const
import network
import time
import urequests

import config


SCAN_TIME_MS = const(10 * 1000)
_IRQ_SCAN_RESULT = const(5)
ADV_SCAN_IND = const(0x02)
ADV_NONCONN_IND = const(0x03)
ADV_TYPES = (ADV_SCAN_IND, ADV_NONCONN_IND)

deduplicator = {}
DEDUPE_TIME = const(2)  # seconds

# Create network
network.WLAN(network.AP_IF).active(False)
sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)
sta_if.connect(config.NETWORK_SSID, config.NETWORK_PASS)
while not sta_if.isconnected():
    pass
print(f"Connected on ip {sta_if.ifconfig()[0]}")


def toggle_leds():
    urequests.get(f"{config.SERVER_ADDR}/leds/toggle")


def bt_irq(event, data):
    if event == _IRQ_SCAN_RESULT:
        addr_type, addr, adv_type, rssi, adv_data = data
        addr = bytes(addr)
        adv_data = bytes(adv_data)
        if adv_type in ADV_TYPES and adv_data[-4:] == config.FINGERPRINT:
            print(f"Event found from {addr}: {adv_data} (type: {adv_type})")
            global deduplicator
            last_hit = deduplicator.get(addr)
            now = time.time()
            if last_hit is None or now - last_hit > DEDUPE_TIME:
                print(f"Hit!")
                deduplicator[addr] = now
                toggle_leds()
            else:
                print("Deduped")


BLE = bluetooth.BLE()
BLE.irq(bt_irq)
BLE.active(True)

while True:
    print("Scanning...")
    BLE.gap_scan(0, 1000 * 1000, 1000 * 1000)
    while True:
        pass
