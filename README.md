# LifX Controller

System to control LifX lights throught the local network without shelling out a small fortune for proprietary products.

BLE Button -> BLE Listener -> Control Centre.

## BLE Button

Battery powered ESP32 with a button attached, running MicroPython 1.17+.

## BLE Listener

Constantly powered ESP32, running MicroPython 1.17+.

## Control Centre

Lightweight web server running locally (eg. Raspberry Pi), running Python 3.8+.
