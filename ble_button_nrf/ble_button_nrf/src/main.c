/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/zephyr.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/pm/pm.h>
#include <zephyr/sys/util.h>
#include <zephyr/sys/printk.h>
#include <inttypes.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/hci.h>

#include <hal/nrf_gpio.h>

static uint8_t mfg_data[] = {0x49, 0x62, 0x84, 0xef};

static const struct bt_data ad[] = {
	BT_DATA(BT_DATA_MANUFACTURER_DATA, mfg_data, 4),
};

#define SLEEP_S 2U
#define ADVERTISE_TIME_MS 2000

/*
 * Get button configuration from the devicetree sw0 alias. This is mandatory.
 */
#define SW0_NODE DT_ALIAS(sw0)
#if !DT_NODE_HAS_STATUS(SW0_NODE, okay)
#error "Unsupported board: sw0 devicetree alias is not defined"
#endif
static const struct gpio_dt_spec button = GPIO_DT_SPEC_GET_OR(
	SW0_NODE,
	gpios,
	{0});

void advertise()
{
	/* Start advertising */
	printk("Advertising for %d ms!\n", ADVERTISE_TIME_MS);
	int err;
	err = bt_le_adv_start(
		BT_LE_ADV_NCONN,
		ad,
		ARRAY_SIZE(ad),
		NULL,
		0);
	if (err)
	{
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}

	k_sleep(K_MSEC(ADVERTISE_TIME_MS));

	err = bt_le_adv_stop();
	if (err)
	{
		printk("Advertising failed to stop (err %d)\n", err);
		return;
	}
}

void main(void)
{
	int ret;

	/* Initialize the Bluetooth Subsystem */
	int err;
	err = bt_enable(NULL);
	if (err)
	{
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	advertise();

	if (!device_is_ready(button.port))
	{
		printk(
			"Error: button device %s is not ready\n",
			button.port->name);
		return;
	}

	nrf_gpio_cfg_input(
		button.pin,
		NRF_GPIO_PIN_PULLUP);
	nrf_gpio_cfg_sense_set(
		button.pin,
		NRF_GPIO_PIN_SENSE_LOW);

	printk("Sleeping");
	
	pm_state_force(0u, &(struct pm_state_info){PM_STATE_SOFT_OFF, 0, 0});

	/* Now we need to go sleep. This will let the idle thread runs and
	 * the pm subsystem will use the forced state. To confirm that the
	 * forced state is used, lets set the same timeout used previously.
	 */
	k_sleep(K_SECONDS(SLEEP_S));

	printk("ERROR: System off failed\n");
	while (true) {
		/* spin to avoid fall-off behavior */
	}
}
